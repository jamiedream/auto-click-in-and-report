#!/usr/bin/env python3

## Import Package
import config
import pymsteams
from skpy import Skype

fieldAccount = "TextBox1"
fieldPassword = "TextBox2"
fieldSubmit = "ImageButton1"

class Attendance():    
    def __report_success__(self, today):
        ## TODO
        skype = Skype(config.sAccount, config.sPassword)
        channel = skype.chats.chat(config.skypeGroupId)
        channel.sendMsg(today + ":" + config.name) 
        myTeamsMessage = pymsteams.connectorcard(config.teamsConnector)
        myTeamsMessage.text(today + ":" + config.name)
        myTeamsMessage.send()

    def __check__(self, chrome):
        chrome.get(config.attendanceUrl)
        employeeNumber = chrome.find_element_by_name(fieldAccount)
        password = chrome.find_element_by_name(fieldPassword)
        employeeNumber.send_keys(config.personalEmployeeNumber)
        password.send_keys(config.personalPassword)
        btn = chrome.find_element_by_name(fieldSubmit)
        btn.click()