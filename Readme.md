### Reference

- Env:
    - Selenium: https://www.twblogs.net/a/5ca2cc09bd9eee5b1a06cdd0
    - Chrome driver: https://sites.google.com/a/chromium.org/chromedriver/downloads
    - Skype: https://stackoverflow.com/questions/56253531/how-to-send-an-automated-message-to-a-skype-group-chat-using-python
    - Teams: https://www.hull1.com/scriptit/2020/08/18/python-to-teams.html

- Preparation:
    1. Setup your computer with Python3, Selenium, Chrome driver(or any other driver of browser which could be work with Selenium), plugins which including skpy and pymsteams.
    2. Join channel on both Skype and Teams.
    3. Build your own config file with parameters:
        ## Basic info
        - attendanceUrl = "url for clock in"
        - teamsConnector = "webhook url of Connector on Teams"
        - skypeGroupId = "Group ID of Skype"
        ## For clock in
        - personalEmployeeNumber = "Office number"
        - personalPassword = "your ID"
        ## For skype
        - sAccount = "account of skype"
        - sPassword = "password of skype"
        ## Report message
        - name = "your name"
        

- Known issue: 
    - Teams message will delayed 5~10 mins.