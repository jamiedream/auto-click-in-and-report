from attendance import Attendance
from selenium import webdriver
from datetime import datetime

attendance = Attendance()
chrome = webdriver.Chrome()
today = datetime.now().strftime('%Y-%m-%d')
attendance.__check__(chrome)
confirm = chrome.switch_to.alert
if confirm.text == "{date} 線上打卡成功".format(date = today):
    confirm.accept()
    chrome.close()
    chrome.quit